public class Main {

	public static void main (String[] args){

		Directory r1 = new Directory("racine");
		Directory r2 = new Directory("documents");
		System.out.println(r1.toString());
		r1.add(r2);
		System.out.println(r2.toString());
		Directory r3 = new Directory("sousdocuments");
		r2.add(r3);
		Directory r4 = new Directory("documentsbis");
		r1.add(r4);
		System.out.println(r3.toString());
		System.out.println(r4.toString());
		File f1 = new File("fichier1", 10);
		File f2 = new File("fichier2", 3);
		File f3 = new File("fichier3", 9);
		r1.add(f1);
		r2.add(f2);
		r4.add(f3);
		r4.add(f2);
		System.out.println(r1.getSize());
		System.out.println(r4.getSize());
	}
	
}